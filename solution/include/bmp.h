#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>


#pragma pack(push, 1)


struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

/*  deserializer   */
enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_COMPRESSION,
    READ_INVALID_PLANES,
    READ_INVALID_FILE,
    READ_MEMORY_ERROR,
    READ_ERROR
};

enum read_status from_bmp( FILE* in, struct image* img );

/*  serializer   */
enum  write_status  {
    WRITE_OK = 0,
    WRITE_ERROR
    /* коды других ошибок  */
};

enum write_status to_bmp( FILE* out, struct image const* img );


bool read_header(FILE* file, struct bmp_header* header );
enum read_status validate_header(struct bmp_header* header);
struct bmp_header* create_header(struct image const* img);
uint8_t size_of_padding_v2(uint32_t width);

#endif //BMP_H

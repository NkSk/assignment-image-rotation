#include "bmp.h"
#include "malloc.h"
#include "string.h"
#include <stdio.h>


#define BF_TYPE 19778
#define BIT_DEPTH 24
#define NO_COMPRESSION 0

/*
 * Main reading from bmp function.
 * Returns result of reading in enum.
 */
enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header *bmp_header = (struct bmp_header *) malloc(sizeof(struct bmp_header));
    if(bmp_header == NULL){
        fclose(in);
        return READ_MEMORY_ERROR;
    }
    if (!(read_header(in, bmp_header))) {
        fclose(in);
        return READ_ERROR;
    }
    if (validate_header(bmp_header) != READ_OK) {
        fclose(in);
        return READ_INVALID_HEADER;
    }
    // assigning values from the header and allocating memory for image data in bytes.
    img->width = bmp_header->biWidth;
    img->height = bmp_header->biHeight;
    img->data = (struct pixel *) malloc(3 * bmp_header->biWidth * bmp_header->biHeight);
    //creating padded width to use in reading
    uint8_t padding = size_of_padding_v2(img->width);
    uint32_t padded_width = sizeof(struct pixel) * img->width + padding;
    //creating offset for header (and rows that were handled)
    //  and going throw the bytes after it in amount of default width (reading every 3 bytes = pixel).
    //  *not swapping pixels (like BGR -> RGB) because also we'll need to do this operation later.
    //      in that case we won't need to swap pixels later.
    for(uint32_t i = 0; i < bmp_header->biHeight; i++){
        if(fseek(in, (int32_t) (sizeof(struct bmp_header) + padded_width * i), SEEK_SET) != 0){
            free(bmp_header);
            fclose(in);
            return READ_ERROR;
        }

        if(fread(img->data + i * img->width, 3, img->width, in) != img->width){
            free(bmp_header);
            fclose(in);
            return READ_ERROR;
        }
    }
    //freeing the allocated memory
    free(bmp_header);
    fclose(in);

    return READ_OK;
}
/*
 * Reader for header of image
 * Returns bool result of work
 */
bool read_header(FILE *file, struct bmp_header *header) {
    if(fread(header, sizeof(struct bmp_header), 1, file) == 1)
        return true;
    else
        return false;
}
/*
 * Header validator function. Checks the most necessary parts of the header.
 * Return enum result of work.
 */
enum read_status validate_header(struct bmp_header* header){
    if (header->bfType != BF_TYPE)
    {
        return READ_INVALID_SIGNATURE ;
    }
    if (header->biBitCount != BIT_DEPTH)
    {
        return READ_INVALID_BITS;
    }
    if(header->biCompression != NO_COMPRESSION){
        return READ_INVALID_COMPRESSION;
    }
    if(header->biPlanes!= 1){
        return READ_INVALID_PLANES;
    }
    return READ_OK;
}

/*
 * Header creator function. Creates a header from template using image params.
 * Returns new header.
 */
struct bmp_header* create_header(struct image const* img){
    struct bmp_header* header = (struct bmp_header*) malloc(sizeof (struct bmp_header));
    header->bfType = BF_TYPE;
    header->biBitCount = BIT_DEPTH;
    header->bOffBits = (uint32_t) sizeof (struct bmp_header);
    header->biCompression = NO_COMPRESSION;
    header->biWidth = img->width;
    header->biHeight = img->height;
    header->biSize = 40;
    header->biPlanes = 1;
    header->biSizeImage = 3*img->width * img->height;
    header->bfileSize = sizeof(struct bmp_header) + (3*img->width + size_of_padding_v2(img->width))*img->height;
    header->bfReserved = 0;
    header->biXPelsPerMeter = 2834;
    header->biYPelsPerMeter = 2834;
    header->biClrUsed = 0;
    header->biClrImportant = 0;

    return header;
}

/*
 * Main writing to bmp file function.
 * Return result of work in enum
 */
enum write_status to_bmp(FILE *out, const struct image *img) {
    //creating header from template and setting the padding size
    struct bmp_header* header = create_header(img);
    uint8_t padding_size = size_of_padding_v2(img->width);
    uint32_t pad_byte = 0;
    //writing whole header to the file
    fwrite(header, sizeof (struct bmp_header),1, out);
    //after the header we write every row into the file (copying pixels from the source and setting padding-bytes as 0)
    for(uint32_t i = 0; i  < img->height; i++){
        if(fwrite(img->data + i * img->width, 3*img->width, 1, out) != 1){
            free(header);
            return WRITE_ERROR;
        }

        if(fwrite(&pad_byte, padding_size, 1, out) != 1){
            free(header);
            return WRITE_ERROR;
        }
    }

    free(header);
    return WRITE_OK;
}

/*
 * Simple function to get padding bytes.
 * Returns amount of bytes.
 */
uint8_t size_of_padding_v2(uint32_t width){
    uint64_t default_row = sizeof(struct pixel) * width;
    if(default_row % 4 == 0){
        return (uint64_t) 0;
    }
    else{
        return 4 - default_row % 4;
    }
}

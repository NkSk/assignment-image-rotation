#include "bmp.h"
#include <malloc.h>

/*
 * Simple function to allocate memory to an image and set basic params.
 * Return new image.
 */
struct image create_image(struct image* source){
    struct image new_image = {
            .width = source->height,
            .height = source->width,
            .data = malloc(source->width * source->height * 3)
    };

    return new_image;
}
/*
 * Main rotating method. Rotating 90 degrees clockwise.
 * Return rotated image.
 */
struct image rotate(struct image* source) {
    struct image new_image = create_image(source);
    if(new_image.data != NULL)
        for(size_t i = 0; i < new_image.height; i++){
            for(size_t j = 0; j < new_image.width; j++){
                new_image.data[j + i * new_image.width] = source->data[(source->height-j-1) * source->width + i];
            }
        }
    else{
        free_image(new_image);
        return *source;
    }
    return new_image;
}

/*
 * Simple function to free the memory that was allocated to the image.
 * Return void.
 */
void free_image(struct image image){
    image.width = 0;
    image.height = 0;
    free(image.data);
}

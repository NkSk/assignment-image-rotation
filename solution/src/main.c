#include "bmp.h"
#include <malloc.h>
#include <stdio.h>

int main(int argc, char** argv) {
    (void) argc; (void) argv;
    if(argc != 3)
        return 0;
    FILE* file_in;

    // Trying to open source-image file.
    if(!(file_in = fopen(argv[1], "rb"))){
        printf("Opening file error.");
        fclose(file_in);
        return 0;
    }
    //Allocating memory for image.
    struct image image = {0};

    // Init reading from bmp function.
    if(from_bmp(file_in, &image) != READ_OK){
        printf("Input file reading error\n");
        return 0;
    }

    // Creating a rotated image.
    struct image rotated_image = {0};
    rotated_image = rotate(&image);

    //Trying to open output file for result.
    FILE* file_out;
    if(!(file_out = fopen(argv[2], "wb"))){
        printf("Output file opening error\n");
        return 0;
    }

    //Init writing to bmp function
    if(to_bmp(file_out, &rotated_image) != WRITE_OK){
        printf("Output file writing error\n");
        return 0;
    }

    //Freeing allocated memory.
    free_image(rotated_image);
    free_image(image);

    return 0;
}
